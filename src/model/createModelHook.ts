import { useLocalStore } from 'mobx-react-lite'

export type AnyModel = object

export type ModelFactory<Model extends AnyModel> = (
  initialAttributes?: Partial<Model>
) => Model

export type ModelHook<Model extends AnyModel> = (
  initialAttributes?: Partial<Model>
) => Model

export function createModelHook<Model extends AnyModel>(
  defaultFactory: () => Model
): ModelHook<Model> {
  const createModel = createModelFactory(defaultFactory)

  return (initialAttributes = {}) =>
    useLocalStore(createModel, initialAttributes)
}

function createModelFactory<Model extends AnyModel>(
  defaultFactory: () => Model
): ModelFactory<Model> {
  return (initialAttributes = {}) => ({
    ...defaultFactory(),
    ...initialAttributes
  })
}
