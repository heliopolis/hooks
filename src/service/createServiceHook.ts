import { useLocalStore } from 'mobx-react-lite'

import { ArgsType, AnyArgs } from '../types'

export type AnyService = object

export function createServiceHook<
  Factory extends (...args: AnyArgs) => AnyService
>(factory: Factory): Factory {
  const hook = (...deps: ArgsType<Factory>) =>
    useLocalStore(createUnary(factory), deps)

  return hook as any
}

function createUnary<Fn extends (...args: AnyArgs) => any>(fn: Fn) {
  return (args: ArgsType<Fn>): ReturnType<Fn> => fn(...args)
}
