import { renderHook } from '@testing-library/react-hooks'

import { ActorHook, createActorHook } from './createActorHook'
import * as Actor from './useActor'

jest.mock('./useActor')

interface Employee {
  hiredAt?: Date
  name: string
}

function createEmployee(): Employee {
  return {
    name: 'James Raynor'
  }
}

const reducers = {
  fire(employee: Employee): Employee {
    return {
      ...employee,
      hiredAt: undefined
    }
  },
  rename(employee: Employee, newName: Employee['name']): Employee {
    return {
      ...employee,
      name: newName
    }
  },
  update(
    employee: Employee,
    newName: Employee['name'],
    date: Date = new Date()
  ): Employee {
    return {
      ...employee,
      hiredAt: date,
      name: newName
    }
  }
}

describe('ActorHook', () => {
  let create: jest.Mock<Employee, [string]>
  let useEmployee: ActorHook<typeof create, typeof reducers>

  beforeEach(() => {
    create = jest.fn(name => ({ ...createEmployee(), name }))
    ;(Actor.useActor as jest.Mock).mockImplementation(() => {})
    useEmployee = createActorHook(create, reducers)
  })

  it('passes its arguments along to the provided `initialize` factory', () => {
    const initialName = 'Zeratul'

    renderHook(() => useEmployee(initialName))

    expect(create).toHaveBeenCalledWith(initialName)
  })

  it('provides `reducers` from `createActorHook` to `useActor` as `reducers`', () => {
    renderHook(() => useEmployee('Grace Hopper'))

    expect(Actor.useActor).toHaveBeenCalledWith(reducers, expect.anything())
  })

  it('provides the value returned from `initialize` to `useActor` as `initialState`', () => {
    const initialState = create('Grace Hopper')
    renderHook(() => useEmployee('Grace Hopper'))

    expect(Actor.useActor).toHaveBeenCalledWith(expect.anything(), initialState)
  })
})
