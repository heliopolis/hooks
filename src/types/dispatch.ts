import { AnyActionType, Action } from './action'
import { AnyArgs, ArgsType } from './function'
import { AnyCaseReducers, CaseReducer } from './reducer'
import { First } from './array'
import { ValueOf } from './object'

export type AnyDispatchers = Record<string, Dispatcher>

export type Dispatcher<Args extends [] | [any] = [any]> = (
  ...args: Args
) => void

export type DispatcherFrom<
  State,
  Reducer extends CaseReducer<State>
> = Reducer extends CaseReducer<State, infer Args>
  ? Dispatcher<Args>
  : () => void

export type DispatchersFrom<State, Reducers extends AnyCaseReducers> = {
  [Type in keyof Reducers & AnyActionType]: DispatcherFrom<
    State,
    Reducers[Type]
  >
}

export type ActionsFromDispatchers<
  Dispatchers extends AnyDispatchers
> = ValueOf<
  {
    [Type in keyof Dispatchers & string]: Action<
      Type,
      First<ArgsType<Dispatchers[Type]>>
    >
  }
>
