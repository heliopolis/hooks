import { createActorHook } from '../actor'
import { ArgsType } from '../types'

type State = boolean

const defaultState = true

function createBoolean(initialState = defaultState): State {
  return initialState
}

interface Dispatchers {
  /**
   * Sets the state of the boolean to `false`
   */
  disable(): void

  /**
   * Sets the state of the boolean to `true`
   */
  enable(): void

  /**
   * Inverts the state of the boolean from `true` to `false` or from `false` to
   * `true`
   */
  toggle(): void

  /**
   * Sets the state of the boolean to the provided `newState`
   * @param newState
   */
  update(newState: State): void
}

// @note the type signature of `useBoolean` is only defined explicitly to
//   provide the documentation from the `Disptachers` interface above.  This is
//   not strictly necessary as the types can be inferred from the provided
//   `reducers`.
export const useBoolean: (
  ...args: ArgsType<typeof createBoolean>
) => [State, Dispatchers] = createActorHook(createBoolean, {
  disable() {
    return false
  },
  enable() {
    return true
  },
  toggle(state: State) {
    return !state
  },
  update(_: State, newState: State) {
    return newState
  }
})
