import { AnyCaseReducers, ArgsType, DispatchersFrom } from '../types'
import { useActor } from './useActor'

export type ActorHook<
  Initializer extends (...args: any[]) => any,
  Reducers extends AnyCaseReducers<ReturnType<Initializer>>
> = (
  ...args: ArgsType<Initializer>
) => [
  ReturnType<Initializer>,
  DispatchersFrom<ReturnType<Initializer>, Reducers>
]

export function createActorHook<
  Initializer extends (...args: any[]) => any,
  Reducers extends AnyCaseReducers<ReturnType<Initializer>>
>(
  initialize: Initializer,
  reducers: Reducers
): ActorHook<Initializer, Reducers> {
  type State = ReturnType<Initializer>

  function useHook(
    ...args: ArgsType<Initializer>
  ): [State, DispatchersFrom<State, Reducers>] {
    return useActor<State, Reducers>(reducers, initialize(...args))
  }

  return useHook // (...args: ArgsType<Initializer>) => useActor(reducers, initialize(...args))
}
