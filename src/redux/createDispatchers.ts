import { Dispatch, useCallback } from 'react'

import { ActionsFrom, AnyCaseReducers, DispatchersFrom } from '../types'

export function createDispatchers<
  State,
  Reducers extends AnyCaseReducers<State>
>(
  actionTypes: Array<keyof Reducers & string>,
  dispatch: Dispatch<ActionsFrom<Reducers>>
): DispatchersFrom<State, Reducers> {
  return actionTypes.reduce<DispatchersFrom<State, Reducers>>(
    (dispatchers, type) => ({
      ...dispatchers,
      [type]: useCallback((payload: any) => dispatch({ payload, type }), [
        dispatch
      ])
    }),
    {} as any
  )
}
