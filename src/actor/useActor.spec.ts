import { act, renderHook } from '@testing-library/react-hooks'

import { useActor } from './useActor'

interface Employee {
  hiredAt?: Date
  name: string
}

function createEmployee(): Employee {
  return {
    name: 'James Raynor'
  }
}

const reducers = {
  fire(employee: Employee): Employee {
    return {
      ...employee,
      hiredAt: undefined
    }
  },
  rename(employee: Employee, newName: Employee['name']): Employee {
    return {
      ...employee,
      name: newName
    }
  },
  update(employee: Employee, newName: Employee['name']): Employee {
    return {
      ...employee,
      name: newName
    }
  }
}

describe(useActor, () => {
  function useEmployee(employee = createEmployee()) {
    return useActor(reducers, employee)
  }

  describe('#state', () => {
    it('returns initial state', async () => {
      const iniitalState = createEmployee()

      const { result } = renderHook(() => useEmployee(iniitalState))
      const [employee] = result.current

      expect(employee).toEqual(iniitalState)
    })
  })

  describe('#dispatchers', () => {
    it('has a dispatcher for every reducer', () => {
      const { result } = renderHook(useEmployee)

      const [, dispatchers] = result.current

      expect(Object.keys(dispatchers)).toEqual(Object.keys(reducers))
    })

    it('calls the reducer with the same name', () => {
      const reducerSpy = jest.spyOn(reducers, 'rename')
      const { result } = renderHook(useEmployee)

      const [, { rename }] = result.current

      act(() => rename('Zergling'))

      expect(reducerSpy).toHaveBeenCalled()
    })

    describe('called reducer', () => {
      it('receives the current state and all arguments passed to the dispatcher', () => {
        const reducerSpy = jest.spyOn(reducers, 'update')
        const { result } = renderHook(useEmployee)

        const [currentState, { update }] = result.current

        const newName = 'Sarah Kerrigan'
        act(() => update(newName))

        expect(reducerSpy).toHaveBeenCalledWith(currentState, newName)
      })
    })

    it('updates current state if updated by the called reducer', () => {
      const initialState = createEmployee()
      const newName = 'Sarah Kerrigan'

      const updatedState = reducers.update(initialState, newName)

      const { result } = renderHook(() => useEmployee(initialState))
      const [, { update }] = result.current

      act(() => update(newName))

      expect(result.current[0]).toEqual(updatedState)
    })
  })
})
