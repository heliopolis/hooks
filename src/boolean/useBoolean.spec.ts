import { act, renderHook } from '@testing-library/react-hooks'

import { useBoolean } from './useBoolean'

describe('useBoolean', () => {
  describe('#state', () => {
    it('returns `true` if no `initialState` is provided', () => {
      const { result } = renderHook(() => useBoolean())

      expect(result.current[0]).toBe(true)
    })

    it('returns `initialState` when provided', () => {
      const { result } = renderHook(() => useBoolean(false))

      expect(result.current[0]).toBe(false)
    })
  })

  describe('#disable', () => {
    it('sets state to `false` if `true`', () => {
      const { result } = renderHook(() => useBoolean(true))

      act(() => result.current[1].disable())

      expect(result.current[0]).toBe(false)
    })

    it('does not change state if already `false`', () => {
      const { result } = renderHook(() => useBoolean(false))

      act(() => result.current[1].disable())

      expect(result.current[0]).toBe(false)
    })
  })

  describe('#enable', () => {
    it('sets state to `true` if `false`', () => {
      const { result } = renderHook(() => useBoolean(false))

      act(() => result.current[1].enable())

      expect(result.current[0]).toBe(true)
    })

    it('does not change state if already `true`', () => {
      const { result } = renderHook(() => useBoolean(true))

      act(() => result.current[1].enable())

      expect(result.current[0]).toBe(true)
    })
  })

  describe('#toggle', () => {
    it('sets state to `true` if `false`', () => {
      const { result } = renderHook(() => useBoolean(true))

      act(() => result.current[1].toggle())

      expect(result.current[0]).toBe(false)
    })

    it('sets state to `false` if `true`', () => {
      const { result } = renderHook(() => useBoolean(false))

      act(() => result.current[1].toggle())

      expect(result.current[0]).toBe(true)
    })
  })

  describe('#update', () => {
    it('sets state to the given `newState`', () => {
      const { result } = renderHook(() => useBoolean(true))

      act(() => result.current[1].update(false))

      expect(result.current[0]).toBe(false)

      act(() => result.current[1].update(true))

      expect(result.current[0]).toBe(true)
    })
  })
})
