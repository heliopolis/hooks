import { createReducer } from './createReducer'

describe(createReducer, () => {
  describe('Reducer', () => {
    interface Human {
      name: string
      birthdate: Date
    }

    function createHuman(): Human {
      return {
        name: 'Unnamed person',
        birthdate: new Date()
      }
    }

    const reducers = {
      rename(current: Human, newName: Human['name']): Human {
        return {
          ...current,
          name: newName
        }
      },
      reset(): Human {
        return createHuman()
      }
    }

    let jamesRaynor: Human

    const reduceHuman = createReducer<Human, typeof reducers>(reducers)

    beforeEach(() => {
      jamesRaynor = createHuman()
    })

    it('calls the case reducer whose key matches the action `type`', () => {
      const resetSpy = jest.spyOn(reducers, 'reset')

      reduceHuman(jamesRaynor, { payload: undefined, type: 'reset' } as any)

      expect(resetSpy).toHaveBeenCalled()
    })

    it('returns the state unchanged if no case reducer could be found matching the action `type`', () => {
      const result = reduceHuman(jamesRaynor, {} as any)

      expect(result).toBe(jamesRaynor)
    })

    it('calls the matching case reducer with the actions payload as arguments', () => {
      const renameSpy = jest.spyOn(reducers, 'rename')

      const newName = 'Sarah Kerrigan'
      reduceHuman(jamesRaynor, { payload: newName, type: 'rename' })

      expect(renameSpy).toHaveBeenCalledWith(jamesRaynor, newName)
    })

    it('throw an error if called with `undefined` state and no state initializer was provided', () => {
      const reducer = createReducer<Human, typeof reducers>(reducers)

      expect(() =>
        reducer(undefined, { type: 'reset', payload: undefined } as any)
      ).toThrow()
    })

    it('uses a fresh state from the provided state initializer if called with `undefined` state', () => {
      const stateInitializer = jest.fn(createHuman)
      const reducer = createReducer(reducers, stateInitializer)

      reducer(undefined, { type: 'reset', payload: undefined } as any)

      expect(stateInitializer).toHaveBeenCalled()
    })
  })
})
