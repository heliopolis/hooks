## [0.4.1](http://gitlab.com/heliopolis/hooks/compare/v0.4.0...v0.4.1) (2020-03-14)

# [0.4.0](http://gitlab.com/heliopolis/hooks/compare/v0.3.0...v0.4.0) (2020-03-13)


### Features

* **actor:** Extracts actor internals to re-usable factories ([e39a01f](http://gitlab.com/heliopolis/hooks/commit/e39a01fcd4196f2994bdc3de63cce9339e6b27b9))
* **model:** Introduces a `ModelHook` factory ([1807ca2](http://gitlab.com/heliopolis/hooks/commit/1807ca287bf41dee6e1ad8fc9d775187a42fca29))
* **service:** Introduces a `ServiceHook` factory ([99c3533](http://gitlab.com/heliopolis/hooks/commit/99c35330b48cea913e0519c83bc999be19b8781d))

# [0.3.0](http://gitlab.com/heliopolis/hooks/compare/v0.2.3...v0.3.0) (2020-03-07)


### Features

* Introduces the `useBoolean` hook ([bfb22e5](http://gitlab.com/heliopolis/hooks/commit/bfb22e5950dc6e9b4f9a6b5dd66b038589685d10))

## [0.2.3](http://gitlab.com/heliopolis/hooks/compare/v0.2.2...v0.2.3) (2020-03-06)


### Bug Fixes

* **ci:** Removes Gitlab release addition from CI ([5d10821](http://gitlab.com/heliopolis/hooks/commit/5d10821467b8e9b4dcca79d1b33e01109f25b5e7))

## [0.2.2](http://gitlab.com/heliopolis/hooks/compare/v0.2.1...v0.2.2) (2020-03-06)


### Bug Fixes

* **ci:** Add Gitlab release to CI ([26c1260](http://gitlab.com/heliopolis/hooks/commit/26c1260db668aa996954faf95eb5e264266d6bd7))

## [0.2.1](http://gitlab.com/heliopolis/hooks/compare/v0.2.0...v0.2.1) (2020-03-06)


### Bug Fixes

* **ci:** Excludes `jest` tests on `node:12` unless needed ([d64487b](http://gitlab.com/heliopolis/hooks/commit/d64487bd73f3789f0c48b85775c16ca1701db094))
* **ci:** Installs `git` package for `release:npm` ([1af08ac](http://gitlab.com/heliopolis/hooks/commit/1af08ac0df8e27146a53e28336b10ff1be28719b))
* **ci:** Removes `--silent` from yarn commands ([df99c8b](http://gitlab.com/heliopolis/hooks/commit/df99c8b6a53c50b53b748553670e02a59af69d4d))
* **ci:** Reorders release config ([bdfd42d](http://gitlab.com/heliopolis/hooks/commit/bdfd42d392521ccfdf3e9318f2ad79c8996ec244))
* **ci:** Updates `CHANGELOG.md` with every release ([6170967](http://gitlab.com/heliopolis/hooks/commit/617096701db091e35f5b7e22f3dbbf9e0562293e))
