export type AnyActionPayload = any
export type AnyActionType = string

export type Action<
  Type extends AnyActionType = AnyActionType,
  Payload = AnyActionPayload
> = {
  payload: Payload
  type: Type
}
