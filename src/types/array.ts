export type First<Ary extends Array<any>> = Ary extends [
  infer Element,
  ...Array<any>
]
  ? Element
  : never
