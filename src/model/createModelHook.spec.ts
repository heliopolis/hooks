import { renderHook } from '@testing-library/react-hooks'

import { createModelHook } from './createModelHook'

describe('ModelHook', () => {
  interface Person {
    birthdate: Date
    name: string
  }

  function createPerson(): Person {
    return {
      birthdate: new Date(),
      name: 'Unnamed person'
    }
  }

  const useModel = createModelHook(createPerson)

  it('uses defaults when no `initialAattributes` are provided', () => {
    const defaults = { customProp: Symbol('WAT?') }
    const hook = createModelHook(jest.fn(() => defaults as any))
    const { result } = renderHook(() => hook())

    expect(result.current).toEqual(defaults)
  })

  it('overrides defaults with provided `initialAttributes`', () => {
    const providedAttributes = { birthdate: new Date() }
    const { result } = renderHook(() => useModel(providedAttributes))

    expect(result.current).toEqual({ ...createPerson(), ...providedAttributes })
  })
})
