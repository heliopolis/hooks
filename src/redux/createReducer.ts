import { AnyCaseReducers, ActionsFrom } from '../types'

export function createReducer<State, Reducers extends AnyCaseReducers<State>>(
  reducers: Reducers,
  initializeState?: () => State
) {
  return (
    prevState: State | undefined,
    action: ActionsFrom<Reducers>
  ): State => {
    const reduce = reducers[action.type]

    if (prevState === undefined) {
      if (initializeState === undefined) {
        throw new Error(
          'Reducer called with `undefined` state and no state initializer was provided'
        )
      }

      prevState = initializeState()
    }

    if (typeof reduce !== 'function') {
      return prevState
    }

    return reduce(prevState, action.payload)
  }
}
