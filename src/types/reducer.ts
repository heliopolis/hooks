import { Action, AnyActionType } from './action'
import { AnyArgs, ArgsType } from './function'
import { ValueOf } from './object'
import { Dispatcher } from './dispatch'
import { Reducer } from 'react'
import { First } from './array'

export type AnyState = any

export type CaseReducer<State = AnyState, Args extends [] | [any] = [any]> = (
  prevState: State,
  ...args: Args
) => State

export type CaseReducersFrom<
  State,
  Dispatchers extends Record<string, Dispatcher>
> = {
  [Name in keyof Dispatchers & string]: Reducer<
    State,
    Action<Name, ArgsType<Dispatchers[Name]>>
  >
}

export type AnyCaseReducers<State = AnyState> = Record<
  AnyActionType,
  CaseReducer<State>
>

export type ActionsFrom<Reducers extends AnyCaseReducers> = ValueOf<
  {
    [Type in keyof Reducers & AnyActionType]: Action<
      Type,
      Reducers[Type] extends CaseReducer<AnyState, infer Args>
        ? First<Args>
        : never
    >
  }
>
