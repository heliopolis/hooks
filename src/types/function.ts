export type AnyArgs = Array<any>

export type ArgsType<Fn extends (...args: AnyArgs) => any> = Fn extends (
  ...args: infer Args
) => any
  ? Args
  : never
