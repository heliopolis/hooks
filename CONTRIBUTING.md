# Contributing

## Installing dependencies

```sh
$ yarn install
```

## Testing locally

This project uses `jest` for testing.  `yarn test` is simply a wrapper for `jest`.  Consult its CLI options.

```sh
$ yarn test

# Collecting coverage
$ yarn test --collectCoverage

# Running in jest's "CI" mode
$ yarn test:ci
```



## Testing CI locally

Sometimes, it is possible for tests to pass locally but fail in the CI.  While these are rare, they can happen.  In such cases, it is often helpful to run CI jobs locally for a faster, narrower feedback loop.

Testing CI jobs locally requires that you have the `gitlab-runner` service installed.  In order to test a given CI job, you need only know its **name**.  This is the key in `.gitlab-ci.yml` that uniquely identifies the job in question.

Currently, job names include:
- `prettier:check`
- `jest:node-10`
- `jest:node-12`
- `jest:node-latest`

```sh
$ gitlab-runner exec docker <job-name>
```

Therefore, if you're looking to test the `jest:node-latest` job...

```sh
$ gitlab-runner exec docker jest:node-latest
```

## Submitted changes to the repo

TBD