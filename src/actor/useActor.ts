import { useReducer } from 'react'

import { AnyCaseReducers, DispatchersFrom } from '../types'
import { createReducer, createDispatchers } from '../redux'

export function useActor<State, Reducers extends AnyCaseReducers<State>>(
  reducers: Reducers,
  initialState: State
): [State, DispatchersFrom<State, Reducers>] {
  const [state, dispatch] = useReducer(
    createReducer<State, Reducers>(reducers),
    initialState
  )

  return [
    state,
    createDispatchers<State, Reducers>(
      // Action types are the associated keys for each _case_ reducer
      Object.keys(reducers),
      dispatch
    )
  ]
}
