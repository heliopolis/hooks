import { act, renderHook } from '@testing-library/react-hooks'
import { observable, runInAction } from 'mobx'

import { createServiceHook } from './createServiceHook'

describe('ServiceHook', () => {
  interface Service {
    mutate(newValue: number): void
    readonly value: number
  }

  interface Box {
    value: number
  }

  describe('dependencyless service', () => {
    function create(): Service {
      const box = observable({ value: 42 })

      return {
        mutate(newValue) {
          box.value = newValue
        },
        get value() {
          return box.value
        }
      }
    }

    const useService = createServiceHook(create)

    it('mutates values', () => {
      const { result } = renderHook(useService)

      act(() => result.current.mutate(97))

      expect(result.current.value).toEqual(97)
    })
  })

  describe('unary service', () => {
    function create(box: Box): Service {
      return {
        mutate(newValue) {
          box.value = newValue
        },
        get value() {
          return box.value
        }
      }
    }

    const useService = createServiceHook(create)

    let box: Box

    beforeEach(() => {
      box = observable({ value: 42 })
    })

    it('mutates state on the dependency', () => {
      const { result } = renderHook(() => useService(box))

      act(() => result.current.mutate(43))

      expect(box.value).toEqual(43)
    })

    it('observes state changes of its dependencies', () => {
      const { result } = renderHook(() => useService(box))

      runInAction(() => {
        box.value = 99
      })

      expect(result.current.value).toEqual(99)
    })
  })

  describe('service with non-observable dependencies', () => {
    function create(box: Box, diff: number): Service {
      return {
        mutate(newValue) {
          box.value = newValue + diff
        },
        get value() {
          return box.value + diff
        }
      }
    }

    const useService = createServiceHook(create)

    let box: Box
    let diff: number

    beforeEach(() => {
      box = observable({ value: 42 })
      diff = 5
    })

    it('mutates state on the dependency', () => {
      const { result } = renderHook(() => useService(box, diff))

      act(() => result.current.mutate(55))

      expect(box.value).toEqual(60)
    })

    it('observes state changes of its dependencies', () => {
      const { result } = renderHook(() => useService(box, diff))

      runInAction(() => {
        box.value = 90
      })

      expect(result.current.value).toEqual(95)
    })
  })
})
