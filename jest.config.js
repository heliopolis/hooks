module.exports = {
  preset: 'ts-jest',
  reporters: [
    'default',
    [
      'jest-junit',
      {
        outputDirectory: './coverage',
        suiteName: 'Jest unit tests'
      }
    ]
  ],
  testEnvironment: 'node',
  verbose: true
}
